; Twitch Chat Notifier
; Copyright (C) 2018 Azzurite
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns twitch-chat-notifier.core
  (:require [clojure.java.io :as io]
            [mount.core :as mnt]
            [taoensso.timbre :as timbre]
            [taoensso.timbre.appenders.3rd-party.rolling :as appender]
            [twitch-chat-notifier.gui.core :refer [gui]]
            [twitch-chat-notifier.gui.events :refer [set-close-handler!]]
            [clojure.string :as str])
  (:import (javafx.application Platform))
  (:gen-class))

(def log-file "log.txt")

(defn delete-old-logs []
  (->> (.list (io/file "."))
    (filter #(str/starts-with? % "log.txt."))
    (sort)
    (reverse)
    (rest) ; keep yesterdays file
    (run! #(io/delete-file % true))))

(defn background-delete-logs []
  (let [delete-files (fn []
                       (while true
                         (delete-old-logs)
                         (Thread/sleep (* 60 (* 60 1000)))))]
    (doto (Thread. delete-files)
      (.setDaemon true)
      (.start))))

(defn add-file-logging []
  (background-delete-logs)
  (timbre/merge-config!
    {:appenders {:spit (assoc (appender/rolling-appender {:path log-file
                                                          :pattern :daily})
                         :output-fn
                         (partial timbre/default-output-fn {:stacktrace-fonts {}}))}}))

(defn -main []
  (set-close-handler! (fn []
                        (Platform/exit)
                        (shutdown-agents)))
  (add-file-logging)
  (mnt/start))
