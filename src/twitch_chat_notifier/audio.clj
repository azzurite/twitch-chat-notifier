; Twitch Chat Notifier
; Copyright (C) 2018 Azzurite
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns twitch-chat-notifier.audio
  (:require [clj-audio.core :refer [with-mixer mixers ->stream default-buffer-size play*]]
            [clj-audio.sampled :refer [make-line ->format with-data-line]]
            [clojure.java.io :as io]
            [clojure.core.async :as a]
            [twitch-chat-notifier.preferences :refer [prefs]]
            [taoensso.timbre :as timbre])
  (:import [java.lang Thread]
           (javax.sound.sampled FloatControl$Type SourceDataLine)))

(timbre/refer-timbre)

(def devices-with-output (filter
                           #(seq (.getSourceLineInfo %))
                           (mixers)))

(def output-mixers (filter
                     #(= SourceDataLine (.getLineClass (first (.getSourceLineInfo %))))
                     devices-with-output))

(def output-mixers-by-name (reduce
                             #(assoc %1 (.getName (.getMixerInfo %2)) %2)
                             {}
                             output-mixers))

(def default-audio-file (io/resource "notification.wav"))

(defn has-error?
  [file-path]
  (try
    (->stream (io/file file-path))
    false
    (catch Exception _
      true)))

(defn set-volume
  [line volume]
  (let [control (.getControl line FloatControl$Type/MASTER_GAIN)
        minimum (.getMinimum control)
        maximum (min 0 (.getMaximum control))
        range (- maximum minimum)
        value (+ minimum (* range (Math/sqrt volume)))]
    (.setValue control value)))

(defn- adjust-volume
  "Adjusts the volume so that a linear scale from 0-1 actually feels linear.
  Adapted from the decibel scale which is 20*log10(Vout/Vin)"
  [volume]
  (let [clamp (fn [n minVal maxVal] (min maxVal (max minVal n)))]
    (clamp (/ (+ (* 20 (Math/log10 (+ volume 0.01)))
                40)
             40.0864)
      0 1)))


(defn play-sound
  "Plays the notification sound."
  [mixer-name volume]
  (let [configured-file-path (:sound-file @prefs)
        stream (->stream (if (has-error? configured-file-path)
                           default-audio-file
                           configured-file-path))
        mixer (get output-mixers-by-name mixer-name)
        line (with-mixer mixer
               (make-line :output
                 (->format stream)
                 default-buffer-size))
        adjusted-volume (adjust-volume volume)]
    (debug "Playing sound with volume" adjusted-volume)
    (a/thread
      (let [fmt (->format stream)]
        (with-data-line [source line fmt]
          (set-volume line adjusted-volume)
          (play* source stream default-buffer-size))))
    ;; Since apparently some lines can be write blocked, play-with never finishes.
    ;; This closes the line and thus makes play-with finish
    (a/thread
      (Thread/sleep 20000)
      (.close line))))



