; Twitch Chat Notifier
; Copyright (C) 2018 Azzurite
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns twitch-chat-notifier.app
  (:require [mount.core :refer [defstate]]
            [twitch-chat-notifier.irc :refer [create-irc-client destroy-irc-client]]
            [twitch-chat-notifier.preferences :refer [prefs]]))

(def messages
  ["Have fun streaming!"
   "Rock the stream!"
   "Enjoy your time :)"
   "Have a nice day!"
   "Consistency is king!"
   "Have a wonderful stream!"])

(defstate app :start (let [app (atom {:irc-client nil
                                      :message (rand-nth messages)
                                      :in-settings? false
                                      :errors #{}
                                      :prefs prefs})]
                       app)
              :stop (let [irc-client (:irc-client @app)]
                      (when (some? irc-client)
                        (destroy-irc-client irc-client))))

(defn start-irc-monitoring [prefs]
  (swap! app assoc :irc-client (create-irc-client prefs)))

(defn stop-irc-monitoring []
  (let [irc-client (:irc-client @app)]
    (destroy-irc-client irc-client)
    (swap! app assoc :irc-client nil)))
