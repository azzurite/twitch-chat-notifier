; Twitch Chat Notifier
; Copyright (C) 2018 Azzurite
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns twitch-chat-notifier.gui.util
  (:require [clojure.edn :as edn]
            [clojure.string :refer [blank? trim split join]]
            [clojure.core.async :refer [chan go go-loop alts! >! timeout]])
  (:import (java.util.function UnaryOperator)
           (javafx.scene.control TextFormatter)))

(defn ignored-users-string->list
  [ignored-users-string]
  (->> (split ignored-users-string #",")
    (map trim)
    (filter (comp not blank?))))

(defn ignored-users-list->string
  [ignored-users-list]
  (join "," ignored-users-list))

(defn int-text-formatter
  ([]
   (int-text-formatter ""))
  ([default]
   (TextFormatter.
     (reify UnaryOperator
       (apply [this change]
         (let [new-text (.getControlNewText change)
               old-text (.getControlText change)
               parse #(try
                        (edn/read-string %)
                        (catch Exception e))
               new-parsed (parse new-text)
               old-parsed (parse old-text)]

           (if (blank? new-text)
             (doto change
               (.setText default))
             (if (and (int? new-parsed) (not= new-parsed old-parsed))
               change
               (doto change
                 (.setText "")
                 (.setRange 0 0))))))))))

(defn- value-changed? [app-state k v]
  (not= v (k @(:prefs @app-state))))

(defn set-pref
  "Update the app state prefs with the given key and value. Returns the value if the preferences have been changed, otherwise returns nil."
  [app-state k v]
  (if (value-changed? app-state k v)
    (swap! app-state (fn [app-state]
                       (swap! (:prefs app-state) assoc k v)
                       app-state))))

(defn update-pref
  "Update the app state prefs at the given key with the given transformation function."
  [app-state k f]
  (swap! app-state (fn [app-state]
                     (swap! (:prefs app-state) update k f)
                     app-state)))

(defn debounce-fn
  "Returns a function that always waits msecs until calling the given func.
  If the returned function is called again during this wait, the timer resets and
  it will again take msecs until func is called."
  [msecs func & args]
  (let [is-running? (atom false)
        timeouts-queue (chan)]
    (fn []
      (go (>! timeouts-queue (timeout msecs)))
      (when (compare-and-set! is-running? false true)
        (go-loop [timeouts []]
          (let [[result _] (alts! (flatten [timeouts-queue timeouts]))
                timeout-finished? (nil? result)
                timeout-added? (some? result)
                timeout-chan result]
            (cond
              timeout-finished? (let [remaining-timeouts (subvec timeouts 1)]
                                  (if (empty? remaining-timeouts)
                                    (do (reset! is-running? false)
                                        (apply func args))
                                    (recur remaining-timeouts)))
              timeout-added? (recur (conj timeouts timeout-chan)))))))))

