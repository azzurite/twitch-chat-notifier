; Twitch Chat Notifier
; Copyright (C) 2018 Azzurite
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns twitch-chat-notifier.gui.events
  (:require [clojure.edn :as edn]
            [clojure.string :refer [blank? trim]]
            [clojure.java.browse :refer [browse-url]]
            [fn-fx.util :refer [run-later]]
            [taoensso.timbre :as timbre]
            [twitch-chat-notifier.app :refer [app start-irc-monitoring stop-irc-monitoring]]
            [twitch-chat-notifier.gui.util :refer [set-pref ignored-users-string->list debounce-fn]]
            [twitch-chat-notifier.gui.main-stage :refer [stage-min-height]]
            [twitch-chat-notifier.preferences :refer [prefs]]
            [twitch-chat-notifier.audio :as audio]))

(timbre/refer-timbre)

(def close-handler (atom (fn [])))

(defn set-close-handler!
  [func]
  (reset! close-handler func))

(defn check-channel-name
  "Checks if the channel name is erroneous and updates the error list accordingly. Returns true if there is an error."
  [channel-name app-state]
  (let [has-error (blank? channel-name)]
    (swap!
      app-state
      update
      :errors
      (if has-error conj disj)
      :channel-name)
    has-error))

(defn get-channel-name
  "Gets the channel name out of UI event data"
  [event-data]
  (get-in event-data [:fn-fx/includes :channel-name :text]))

(defn get-new-val
  [event-data]
  (:fn-fx.listen/new event-data))

(defmulti event-handler
  "Handles UI events."
  :event)

(defmethod event-handler :toggle-client
  [_]
  (if (nil? (:irc-client @app))
    (when (not (check-channel-name (:channel-name @prefs) app))
      (start-irc-monitoring prefs))
    (stop-irc-monitoring)))

(defmethod event-handler :name-focus
  [event-data]
  (let [channel-name (get-channel-name event-data)]
    (check-channel-name channel-name app)))

(defmethod event-handler :name-changed
  [event-data]
  (let [channel-name (get-channel-name event-data)]
    (set-pref app :channel-name (trim channel-name))))

(def play-sound-throttled
  (debounce-fn 300 #(audio/play-sound (:sound-device @prefs) (:volume @prefs))))

(defmethod event-handler :sound-device-changed
  [event-data]
  (let [sound-device (get-new-val event-data)]
    (if (set-pref app :sound-device sound-device)
      (play-sound-throttled))))

(defmethod event-handler :volume-changed
  [event-data]
  (let [volume (get-new-val event-data)]
    (if (set-pref app :volume volume)
      (play-sound-throttled))))

(defmethod event-handler :min-time-changed
  [event-data]
  (let [min-time (edn/read-string (get-new-val event-data))]
    (set-pref app :min-time min-time)))

(defmethod event-handler :ignored-users-changed
  [event-data]
  (let [ignored-users-string (get-new-val event-data)
        ignored-users (ignored-users-string->list ignored-users-string)]
    (set-pref app :ignored-users ignored-users)))

(defmethod event-handler :sound-file-changed
  [event-data]
  (let [sound-file (get-new-val event-data)
        error? (and (not (blank? sound-file))
                 (audio/has-error? sound-file))]
    (swap! app update :errors (if error? conj disj) :sound-file)
    (when (and (not error?) (set-pref app :sound-file sound-file))
      (play-sound-throttled))))

(declare gui)

(defmethod event-handler :toggle-settings
  [_]
  (swap! app update :in-settings? not)
  (send gui (fn [old-gui]
              (run-later
                (let [stage @(:root old-gui)]
                  (.sizeToScene stage)
                  (if (< (.getHeight stage) stage-min-height)
                    (.setHeight stage stage-min-height))))
              old-gui)))

(defmethod event-handler :open-url
  [{:keys [url]}]
  (info "Opening browser on" url)
  (browse-url url))

(defmethod event-handler :close-app
  [_]
  (info "User closed app.")
  (@close-handler))

(defmethod event-handler :default
  [{:keys [event] :as event-data}]
  (warn "Unknown UI event" event event-data))
