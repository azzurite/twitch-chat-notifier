; Twitch Chat Notifier
; Copyright (C) 2018 Azzurite
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns twitch-chat-notifier.gui.main-stage
  (:require [fn-fx.diff :refer [defui render]]
            [fn-fx.controls :as ui]
            [taoensso.timbre :as timbre]
            [twitch-chat-notifier.gui.main-pane :refer [main-pane]]
            [twitch-chat-notifier.gui.settings-pane :refer [settings-pane]])
  (:import (javafx.scene.image Image)
           (javafx.stage Stage)))

(timbre/refer-timbre)

(defn actual-app-container
  [{:keys [message] :as args}]
  (ui/border-pane
    :padding (ui/insets
               :bottom 10
               :left 10
               :right 10
               :top 10)
    :top (ui/h-box
           :children [(ui/text
                        :text (if (:in-settings? args)
                                "Additional Settings"
                                message)
                        :font (ui/font
                                :size 20))
                      (ui/stack-pane
                        :h-box/hgrow :always
                        :alignment :center-right
                        :children [(ui/hyperlink
                                     :graphic (ui/image-view
                                                :url (if (:in-settings? args)
                                                       "return.png"
                                                       "settings.png"))
                                     :on-action {:event :toggle-settings})])])
    :center (if (:in-settings? args)
              (settings-pane args)
              (main-pane args))
    :bottom (ui/flow-pane
              :min-height 30
              :pref-width 300
              :alignment :center
              :children [(ui/text
                           :text "Get support on ")
                         (ui/hyperlink
                           :graphic (ui/image-view
                                      :url "twitch.png")
                           :text "AzzuriteTV"
                           :on-action {:event :open-url
                                       :url "https://www.twitch.tv/azzuritetv"})
                         (ui/hyperlink
                           :graphic (ui/image-view
                                      :url "discord.png")
                           :text "Azzurite"
                           :on-action {:event :open-url
                                       :url "https://discord.gg/sd4fNnU"})
                         (ui/text
                           :text " (Just message me)")])))

(defn fix-app-container
  "Workaround for https://github.com/fn-fx/fn-fx/issues/78"
  [args]
  (if (:in-settings? args)
    (ui/stack-pane :children [(actual-app-container args)])
    (actual-app-container args)))

(defui AppContainer
  (render [this args]
    (fix-app-container args)))

(def stage-min-height 260)

(defui MainStage
  (render [this args]
    (ui/stage
      :title "Twitch Chat Notifier"
      :shown true
      :min-width 285
      :min-height stage-min-height
      :width 450
      :height 260
      :max-width 800
      :max-height 350
      :on-close-request {:event :close-app}
      :icons [(Image. "icon.png")]
      :scene (ui/scene
               :stylesheets ["global_styles.css"]
               :root (app-container args)))))
