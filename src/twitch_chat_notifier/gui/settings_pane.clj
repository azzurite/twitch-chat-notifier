; Twitch Chat Notifier
; Copyright (C) 2018 Azzurite
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns twitch-chat-notifier.gui.settings-pane
  (:require [fn-fx.diff :refer [defui render]]
            [fn-fx.controls :as ui]
            [twitch-chat-notifier.gui.fn-fx :refer [my-grid-pane]]
            [twitch-chat-notifier.gui.util :refer [ignored-users-list->string int-text-formatter]]))

(defui SettingsPane
  (render [this {:keys [prefs errors]}]
    (ui/stack-pane
      ;; wrap with stack-pane is a bugfix, because fn-fx otherwise thinks this structure is
      ;; the same as the MainPane and thus doesn't update certain things
      :children [(my-grid-pane
                   :children [(ui/label
                                :text "Ignored usernames (separate with ,):"
                                :wrap-text true
                                :grid-pane/row-index 1
                                :grid-pane/column-index 0)

                              (ui/text-field
                                :id :ignored-users
                                :text (if-let [ignored-users (:ignored-users @prefs)]
                                        (ignored-users-list->string ignored-users)
                                        "")
                                :listen/text {:event :ignored-users-changed}
                                :grid-pane/row-index 1
                                :grid-pane/column-index 1)

                              (ui/label
                                :text "Minimum time without messages before a notification is played (in seconds):"
                                :wrap-text true
                                :grid-pane/row-index 2
                                :grid-pane/column-index 0)

                              (ui/text-field
                                :id :min-time
                                :text (if-let [min-time (:min-time @prefs)]
                                        (str min-time)
                                        "")
                                :listen/text {:event :min-time-changed}
                                :text-formatter (int-text-formatter "0")
                                :grid-pane/row-index 2
                                :grid-pane/column-index 1)

                              (ui/label
                                :text "Custom sound file path (only .wav files supported):"
                                :wrap-text true
                                :grid-pane/row-index 3
                                :grid-pane/column-index 0)

                              (ui/text-field
                                :id :sound-file
                                :text (if-let [sound-file (:sound-file @prefs)]
                                        sound-file
                                        "")
                                :style (if (:sound-file errors)
                                         "-fx-text-box-border: red; -fx-focus-color: red;"
                                         "")
                                :listen/text {:event :sound-file-changed}
                                :grid-pane/row-index 3
                                :grid-pane/column-index 1)])])))
