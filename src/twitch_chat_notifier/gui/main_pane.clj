; Twitch Chat Notifier
; Copyright (C) 2018 Azzurite
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns twitch-chat-notifier.gui.main-pane
  (:require [clojure.string :refer [blank?]]
            [fn-fx.diff :refer [defui render]]
            [fn-fx.controls :as ui]
            [twitch-chat-notifier.gui.fn-fx :refer [my-grid-pane]]
            [twitch-chat-notifier.audio :refer [output-mixers-by-name]]))

(def red
  javafx.scene.paint.Color/DARKRED)
(def green
  javafx.scene.paint.Color/DARKGREEN)

(def mixer-list (cons "Default" (keys output-mixers-by-name)))

(defui MainPane
  (render [this {:keys [irc-client errors prefs]}]
    (my-grid-pane
      :children [(ui/label
                   :text "Sound output device:"
                   :grid-pane/row-index 1
                   :grid-pane/column-index 0)

                 (ui/combo-box
                   :id :sound-device
                   :max-width Double/MAX_VALUE
                   :items mixer-list
                   :value (let [sound-device (:sound-device @prefs)
                                device-stored (not (blank? sound-device))
                                device-in-list (some #(= % sound-device) mixer-list)]
                            (if (and device-stored device-in-list)
                              sound-device
                              "Default"))
                   :listen/value {:event :sound-device-changed}
                   :grid-pane/row-index 1
                   :grid-pane/column-index 1)

                 (ui/label
                   :text "Volume:"
                   :grid-pane/row-index 2
                   :grid-pane/column-index 0)

                 (ui/slider
                   :id :volume
                   :min 0
                   :max 1
                   :value (:volume @prefs)
                   :listen/value {:event :volume-changed}
                   :grid-pane/row-index 2
                   :grid-pane/column-index 1)

                 (ui/label
                   :text "Channel Name:"
                   :grid-pane/row-index 3
                   :grid-pane/column-index 0)

                 (ui/text-field
                   :id :channel-name
                   :text (if-let [channel-name (:channel-name @prefs)]
                           channel-name
                           "")
                   :disable (some? irc-client)
                   :style (if (:channel-name errors)
                            "-fx-text-box-border: red;"
                            "")
                   :listen/focused {:event :name-focus
                                    :fn-fx/include {:channel-name #{:text}}}
                   :listen/text {:event :name-changed
                                 :fn-fx/include {:channel-name #{:text}}}
                   :grid-pane/row-index 3
                   :grid-pane/column-index 1)

                 (ui/text
                   :text (if (some? irc-client) "Chat is being monitored" "Chat monitoring offline")
                   :fill (if (some? irc-client) green red)
                   :grid-pane/row-index 4
                   :grid-pane/column-index 0)

                 (ui/h-box
                   :spacing 10
                   :alignment :bottom-right
                   :children [(ui/button
                                :text (if (some? irc-client) "Stop monitoring" "Monitor chat")
                                :font (ui/font
                                        :family ""
                                        :size 14
                                        :weight :bold)
                                :on-action {:event :toggle-client})]
                   :grid-pane/row-index 4
                   :grid-pane/column-index 1)])))
