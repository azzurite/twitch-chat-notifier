; Twitch Chat Notifier
; Copyright (C) 2018 Azzurite
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns twitch-chat-notifier.gui.fn-fx
  (:require [fn-fx.controls :as ui]))

(defn with-extra-props
  [elem-macro props extra-props]
  (let [final-props (merge props extra-props)]
    `(~elem-macro ~@(mapcat identity final-props))))

(defmacro my-grid-pane
  [& {:as props}]
  (with-extra-props #'ui/grid-pane props
    {:alignment :top-left
     :hgap 10
     :vgap 10
     :pref-width 400
     :column-constraints ['(ui/column-constraints
                             :max-width 220)
                          '(ui/column-constraints
                             :hgrow :always)]}))
