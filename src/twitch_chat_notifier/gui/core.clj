; Twitch Chat Notifier
; Copyright (C) 2018 Azzurite
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns twitch-chat-notifier.gui.core
  (:require [fn-fx.fx-dom :as dom]
            [fn-fx.util :refer [run-later]]
            [mount.core :refer [defstate]]
            [taoensso.timbre :as timbre]
            [twitch-chat-notifier.app :refer [app]]
            [twitch-chat-notifier.gui.events :refer [event-handler]]
            [twitch-chat-notifier.gui.main-stage :refer [main-stage]]
            [twitch-chat-notifier.gui.util :refer :all]))

(timbre/refer-timbre)

(defn create-gui [app-state]
  (let [handler-fn (fn [{:keys [event] :as event-data}]
                     (debug "UI Event" event event-data)
                     (event-handler event-data))

        ui-state (agent (dom/app (main-stage @app-state) handler-fn))]
    (add-watch app-state :ui (fn [_ _ _ _]
                               (send ui-state
                                 (fn [old-ui]
                                   (debug "Redraw" (pr-str @app-state))
                                   (try
                                     (let [stage (main-stage @app-state)]
                                       (dom/update-app old-ui stage))
                                     (catch Exception e
                                       (fatal e "Error while updating GUI")
                                       (throw e)))))))
    (set-error-handler! ui-state (fn [a err]
                                   (set-error-mode! a :continue)))
    ui-state))

(defn destroy-gui [gui]
  (send gui (fn [old-gui]
              (run-later
                (.close @(:root old-gui))))))

(defstate gui :start (create-gui app)
              :stop (destroy-gui gui))
