; Twitch Chat Notifier
; Copyright (C) 2018 Azzurite
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns twitch-chat-notifier.preferences
  (:require [mount.core :refer [defstate]]
            [taoensso.timbre :as timbre]))

(timbre/refer-timbre)

(def node "/tv/azzurite/twitch-chat-notifier")

(defn get-pref-node []
  (.node (java.util.prefs.Preferences/userRoot) node))

(defn add-getter-setter-if-missing
  [m]
  (into {}
    (map
      (fn [[key val]]
        [key (-> val
               (update :getter
                 (fn [val]
                   (if (nil? val)
                     #(.get %1 %2 nil)
                     val)))
               (update :setter
                 (fn [val]
                   (if (nil? val)
                     #(.put %1 %2 %3)
                     val))))])
      m)))

(defn type-double
  [default]
  {:getter #(.getDouble %1 %2 default)
   :setter #(.putDouble %1 %2 %3)})

(defn type-int
  [default]
  {:getter #(.getInt %1 %2 default)
   :setter #(.putInt %1 %2 %3)})

(defn type-list
  []
  {:getter #(read-string (.get %1 %2 "[]"))
   :setter #(.put %1 %2 (pr-str %3))})

(def pref-keys (add-getter-setter-if-missing
                 {:channel-name {:name "channel_name"}
                  :sound-device {:name "sound_device"}
                  :sound-file {:name "sound_file"}
                  :volume (merge {:name "volume"}
                            (type-double 0.5))
                  :ignored-users (merge {:name "ignored_users"}
                                   (type-list))
                  :min-time (merge {:name "min_time"}
                              (type-int 0))}))

(defn get-pref
  [{:keys [name getter]}]
  (let [node (get-pref-node)]
    (getter node name)))

(defn set-pref
  [{:keys [name setter]} val]
  (let [node (get-pref-node)]
    (setter node name val)))


(defn load-prefs []
  (reduce-kv #(assoc %1 %2 (get-pref %3)) {} pref-keys))

(defn save-prefs [prefs]
  (doseq [[k v] prefs]
    (when (and (k pref-keys) v)
      (set-pref (k pref-keys) v))))

(defstate prefs :start (let [prefs (atom (load-prefs))]
                         (add-watch prefs :auto-save (fn [_ _ _ new-val]
                                                       (debug "Updating preferences with " (pr-str new-val))
                                                       (save-prefs new-val)))
                         prefs))

