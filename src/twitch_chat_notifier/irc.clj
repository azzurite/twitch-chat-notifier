; Twitch Chat Notifier
; Copyright (C) 2018 Azzurite
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns twitch-chat-notifier.irc
  (:require [clojure.string :refer [lower-case]]
            [twitch-chat-notifier.audio :refer [play-sound]]
            [taoensso.timbre :as timbre])
  (:import (java.io PrintWriter InputStreamReader BufferedReader)
           (java.net Socket)))

(timbre/refer-timbre)

(defn rand-between [min max]
  (+ (long (rand (- max min))) min))

(def twitch-irc {:name "irc.chat.twitch.tv"
                 :port 6667})
(defn create-user
  "Creates an anonymous user name accepted by Twitch."
  []
  (str "justinfan" (rand-between 10000000000000 99999999999999)))

(defn read-irc
  "Read a message from the connection. Blocking."
  [conn]
  (.readLine (:in conn)))

(defn write-irc
  "Writes a message through the connection."
  [conn msg]
  (debug "SEND > " msg)
  (doto (:out conn)
    (.println (str msg "\r"))
    (.flush)))


(defn login [conn nick]
  "Send an IRC NICK message through the connection."
  (write-irc conn (str "NICK " nick)))

(defn join
  "Send an IRC channel JOIN message through the connection."
  [conn channel]
  (write-irc conn (str "JOIN #" (lower-case channel))))

(defn destroy-irc-client
  "Closes the connection to the Twitch IRC server and disables reconnecting."
  [irc]
  (reset! (:running irc) false)
  (let [socket (:socket @(:conn irc))]
    (if (.isConnected socket)
      (.close socket)))
  (info "IRC client stopped."))

(defn set-connection
  "Connects to the Twitch IRC server."
  [irc channel]
  (info "Connecting to Twitch IRC server...")
  (let [socket (Socket. (:name twitch-irc) (:port twitch-irc))
        in (BufferedReader. (InputStreamReader. (.getInputStream socket)))
        out (PrintWriter. (.getOutputStream socket))
        conn {:socket socket
              :in     in
              :out    out}]
    (reset! (:conn irc) conn)
    (login conn (create-user))
    (join conn channel)
    (info "Connected to Twitch IRC server.")))

(defn connect
  "Connects to the Twitch IRC server if it is not already connected."
  [irc channel]
  (let [conn @(:conn irc)]
    (if (or
          (nil? conn)
          (not (.isConnected (:socket conn))))
      (set-connection irc channel)
      irc)))

(defn message-collector
  "Collects messages from the connection. Blocking."
  [irc]
  (while @(:running irc)
    (let [msg (read-irc @(:conn irc))]
      (send (:messages irc) conj msg))))

(defn manage-connection
  "Starts a connection to the Twitch IRC server and keeps it alive. Collects any messages coming from the server into the :messages agent, as a string."
  [irc channel]
  (doto (Thread. (fn []
                   (while @(:running irc)
                     (connect irc channel)
                     (try
                       (message-collector irc)
                       (catch Exception e
                         (when @(:running irc)
                           (error e "Something bad happened :(")))))))
    (.start)))

(defn handler-applies?
  "Checks if the given handler applies to the message."
  [handler msg]
  (let [match (:match handler)]
    (if (= match :all)
      true
      (re-find match msg))))

(defn message-handler
  "Creates a handler function (fn [msg] ...) that runs all registered and matching handlers for the message."
  [irc handlers]
  (fn [msg]
    (let [consumed (reduce
                     (fn [consumed handler]
                       (if (and (not consumed) (handler-applies? handler msg))
                         (let [new-consumed ((:handler-fn handler) irc msg)]
                           (debug "Handler found:" (:name handler))
                           (debug "Message consumed?" new-consumed)
                           new-consumed)
                         consumed))
                     false
                     handlers)]
      (when-not consumed
        (warn (str "Message has not been consumed: " msg))))))

(defn message-dispatcher
  "Creates a function that can be used as an argument to `add-watch`. Handles all new messages and clears the message queue."
  [irc]
  (fn [key ref old-val new-val]
    (when (peek new-val)
      (let [handler (message-handler irc @(:handlers irc))]
        (send ref
          (fn [messages]
            (doseq [msg messages]
              (handler msg))))))))

(defn add-message-handler
  "Adds a message handler for the IRC client. Handlers always run in the order of their addition.

  Handlers are a map with the following keys:
  `:name` - Name of the handler to be used in logging.
  `:match` - Determines if the handler applies to a message. Either `:all` or a regex. `:all` matches all messages. If the value is a regex, it will be applied to the whole message, if it matches, the handler applies.
  `:handler-fn` - A function that takes two arguments: the irc-client instance and the message. Return true if the message should be consumed and no further handlers applied."
  [irc handler]
  (swap! (:handlers irc) conj handler)
  (add-watch (:messages irc) :message-handler (message-dispatcher irc)))

(defn create-irc-client
  "Creates an IRC client that plays a sound whenever a message is posted in the given channel."
  [prefs]
  (let [irc {:conn     (atom nil)
             :messages (agent clojure.lang.PersistentQueue/EMPTY)
             :handlers (atom [])
             :running  (atom true)}]
    (manage-connection irc (:channel-name @prefs))

    (add-message-handler irc
      {:name       "logger"
       :match      :all
       :handler-fn (fn [irc msg]
                     (debug (str "IRC message received: " msg))
                     false)})

    (add-message-handler irc
      {:name       "ping answerer"
       :match      #"^PING"
       :handler-fn (fn [irc msg]
                     (debug "PONG!")
                     (write-irc @(:conn irc) (str "PONG " (re-find #":.*" msg)))
                     true)})

    (add-message-handler irc
      (let [matcher (re-pattern (str "^:([^ ]*?)![^ ]*? PRIVMSG #" (lower-case (:channel-name @prefs)) " :"))]
        {:name "username filterer"
         :match matcher
         :handler-fn (fn [irc msg]
                       (when-let [[_ user] (re-find matcher msg)]
                         (some #{(lower-case user)} (map lower-case (:ignored-users @prefs)))))}))


    (add-message-handler irc
      {:name       "sound player"
       :match      (re-pattern (str "^:[^ ]*? PRIVMSG #" (lower-case (:channel-name @prefs)) " :"))
       :handler-fn (let [last-message (atom (- (System/currentTimeMillis) (* 1000 (:min-time @prefs))))
                         time-since-last #(- (System/currentTimeMillis) @last-message)
                         sound-should-play? #(> (time-since-last) (* 1000 (:min-time @prefs)))]
                     (fn [irc msg]
                       (debug "Chat message received.")
                       (if (sound-should-play?)
                         (do
                           (info "Playing sound.")
                           (play-sound (:sound-device @prefs) (:volume @prefs)))
                         (debug "Not playing sound. Time since last message (" (quot (time-since-last) 1000) ") less than minimum no-chat time " (:min-time @prefs)))
                       (reset! last-message (System/currentTimeMillis))
                       true))})

    irc))
