(defproject twitch-chat-notifier "1.0.1-SNAPSHOT"
  :description "Plays sound notifications when a user sends a message in a Twitch chat channel"
  :url "https://gitlab.com/azzurite/twitch-chat-notifier"
  :license {:name "GNU General Public License v3.0"
            :url "http://www.gnu.org/licenses/gpl-3.0.html"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [org.clojure/core.async "0.4.500"]
                 ;[fn-fx/fn-fx-openjfx11 "0.5.0-SNAPSHOT"]
                 [fn-fx/fn-fx-javafx "0.5.0-SNAPSHOT"]
                 [org.clojars.beppu/clj-audio "0.3.0"]
                 [mount "0.1.16"]
                 [com.taoensso/timbre "4.10.0"]]
  :main ^:skip-aot twitch-chat-notifier.core
  :target-path "target/%s"
  :uberjar-name "twitch-chat-notifier.jar"
  :profiles {:uberjar {:aot :all
                       :omit-source true}})
